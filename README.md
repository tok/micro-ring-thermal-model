# Thermal model of a silicon micro-ring optical modulator.

## Self-heating

The script `ring_modulator_interactive.py` illustrates the self-heating behaviour of a micro ring resonator. Light absorbed in the ring is turned into heat what is called 'self-heating'. Since the refractive index in the ring is dependent on the temperature the self-heating shifts the resonance towards longer wavelengths. The resonance therefore _escapes_ from a laser that has a shorter wavelength. This can be explored interactively here.

![Screenshot of interactive simulation.](doc/self-heating_screenshot.png)

