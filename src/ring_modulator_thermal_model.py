#!/usr/bin/env python3
#
# Copyright (c) 2018-2020 Thomas Kramer.
#
# This file is part of "silicon micro-ring modulator thermal model" 
# (see https://codeberg.org/tok/micro-ring-thermal-model).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
from scipy.integrate import ode
from scipy.integrate import odeint
from matplotlib import pyplot as plt
import scipy.constants

"""
References:
[1]: A 45 CMOS-SOI Monolithic Photonics Platform With Bit-Statistics-Based Resonant Microring Thermal Tuning

"""

# Density [kg/m^3]
density_silicon = 2329.0

# Specific heat capacity [J/(kg*K)].
spec_heat_capacity_silicon = 710

# Refractive index of the ring at 300K.
ring_n0 = 2

# Laser power [W].
default_laser_power = 1e-3

# Laser wavelength [m]
default_laser_wavelength = 1550e-9

# Environment temperature [K]
t_env = 300

"""
Heating efficiency = 1 nm / mW
Lambda change per temperature: 0.1 nm / K
=> Heat flux to environment: 1e-4 W/K
"""

# Thermal conductances [W/K]
a_env_ring = 1e-4   # 2.73e-4  (calculated from [1], fig. 3)
a_env_heater = 1e-6
a_heater_ring = 1e-3


# Ring dimensions [m]:
ring_height = 80e-9
ring_radius_outer = 5e-6
ring_radius_inner = ring_radius_outer - 1.5e-6
ring_volume = ring_height * (ring_radius_outer**2 - ring_radius_inner**2) * np.pi
ring_mass = ring_volume * density_silicon
ring_heat_capacity = ring_mass*spec_heat_capacity_silicon # [J/K]


# Heat capacities [J/K]
c_heater = 1e-14
c_ring = ring_heat_capacity

"""Thermal model of a ring modulator.
"""
class RingModulator:
    
    def __init__(self, t_env = t_env):
        
        self.drive_voltage = lambda t : 0
        self.heater_power = lambda t : 0
        
        self.A = 0.9                 # Device's intrinsic extinction. A = 1: perfectly critically coupled.
        """
        Q-factor of ring resonator,
        Q = lambda_0/delta_lambda
        delta_lambda = lambda_0 / Q
        
        lambda_0 is the resonant wavelength of the ring
        """
        self.Q = 11600 # [1], Table I
        
        # Wavelength of laser.
        self._lambda_laser = default_laser_wavelength
        self.laser_power = default_laser_power
        
        
        self.lambda_0 = 1550e-9      # Nominal resonance wavelength.
        self.temp_0 = 305.5            # Temperature at which lambda = lambda_0
        
        self._kappa = 0.5 # Portion of light power in ring that is converted into heat. Somewhere between 0.3 and 0.9.
        
        self._sim_state = [0,0]
        self._sim_time = 0
        
        self._t_env = t_env	# Environment temperature
        
        self.simulation_reset(self._t_env)
        
    """
    Reset the simulation state.
    """
    def simulation_reset(self, temperature):
        """
        state of simulation: [
                                temp. ring,
                                temp. heater,
                                low-pass filtered photo current
                            ]
        """
        self._sim_state = [temperature, temperature, 0]
        self._sim_time = 0
    
    """
    Advance the simulation.
    
    hmax = Maximal absolute step allowed.
    """
    def simulation_advance(self, time, hmax=1e-6):
        y0 = self._sim_state
        sol = odeint(self._ode_rhs, y0, time, hmax=hmax)
        
        # Remember state of simulation.
        self._sim_state = sol[-1]
        self._sim_time += time[-1]
        return sol
        
        
    """
    Right hand side of ODE.

    y = State vector.
    t = Time [s]
    """
    def _ode_rhs(self, y, t):
        t_ring = y[0]
        t_heater = y[1]
        
        drive_voltage = self.drive_voltage(t)
        
        return [
            # t_ring' = 
            1./c_ring*(
                a_env_ring*(self._t_env-t_ring)                       # Heat absorbed by bulk material.
                + a_heater_ring*(t_heater-t_ring)               # Power from heater.
                + self.self_heating_power(t_ring, drive_voltage) # Self heating.
            ),
            
            #  t_heater' =
            1./c_heater*(
                self.heater_power(t)
                + a_env_heater*(self._t_env-t_heater) 
                + a_heater_ring*(t_ring-t_heater)
            ),
            
            # low pass filtered photo current
            #1.0/ (R_pd * C_pd) * 0
            0
        
        ]
    
    @property
    def current_ring_temperature(self):
        return self._sim_state[0]
    
    
    def current_resonant_wavelength(self, drive_voltage=0):
        return self.resonant_wavelength(self.current_ring_temperature, drive_voltage)

    def resonant_wavelength(self, ring_temp, drive_voltage):
        """
        Calculate the resonant wavelength at `ring_temp`.
        Frequency change: 10 GHz / K
        Change in wavelength: 0.1 nm / K
        """
        temp_coefficient = 0.1e-9
        
        """
        Change of resonant wavelength per applied voltage:
        d lambda_0 / d voltage = 9 pm / V
        
        (Alloatti, High-speed modulator with interleaved junctions in zero-change CMOS photonics, https://arxiv.org/pdf/1601.05046.pdf)
        
        """
        voltage_coefficient = -9e-12
        
        lambda_eff = self.lambda_0 + (ring_temp - self.temp_0) * temp_coefficient + voltage_coefficient*drive_voltage
        return lambda_eff

    """
    Calculates the fraction of laser power that goes into the ring.
    Equals 1-transmittance.
    
    ring_temp = Temperature of the ring [K].
    drive_voltage = Voltage applied at the modulator [V].
    """
    def _ring_absorption(self, ring_temp, drive_voltage, laser_wavelength = None):
        
        if(laser_wavelength is None):
            laser_wavelength = self._lambda_laser
        
        lambda_0 = self.resonant_wavelength(ring_temp, drive_voltage)
        
        delta_lambda = lambda_0 / self.Q   # Line width of resonance.
        
        return self.A/(1+4*((laser_wavelength-lambda_0)/delta_lambda)**2)

    def absorptionByLaserWavelength(self, drive_voltage, laser_wavelength):
        return self._ring_absorption(self.current_ring_temperature, 0, laser_wavelength)

    """
    Calculates the fraction of laser power that is transmitted through the modulator.
    """
    def transmittance(self, ring_temp, drive_voltage):
        return 1-self._ring_absorption(ring_temp, drive_voltage)

    """
    P_laser = Laser power [W].
    ring_temp = Temperature of the ring [K].
    """
    def self_heating_power(self, ring_temp, drive_voltage):
        Ps = self._kappa * self.laser_power * self._ring_absorption(ring_temp, drive_voltage)
        return Ps
    
    """
    Current output of sensing photo diode.
    """
    def photo_current(self):
        ring_temp = self.current_ring_temperature
        drive_voltage = self.drive_voltage(self._sim_time)
        intensity = self.laser_power * self._ring_absorption(ring_temp, drive_voltage)
        pd_responsivity = 1e-3 # [A/W]
        return intensity * pd_responsivity
        
        



