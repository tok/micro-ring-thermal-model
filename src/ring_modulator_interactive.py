#!/bin/env python3
#
# Copyright (c) 2018-2020 Thomas Kramer.
#
# This file is part of "silicon micro-ring modulator thermal model" 
# (see https://codeberg.org/tok/micro-ring-thermal-model).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from ring_modulator_thermal_model import RingModulator
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation
from matplotlib.widgets import Slider, Button, RadioButtons

def format_axis(ax):
    formatter = plt.ScalarFormatter(useOffset=False)
    ax.yaxis.set_major_formatter(formatter)
    ax.xaxis.set_major_formatter(formatter)

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.2, bottom=0.25)


heaterInit = 0
laserPowerInit = 1e-3

ax.set_title('')
ax.set_xlabel('Wavelength [nm]')
ax.set_ylabel('Absorption')

mod = RingModulator()
mod.laser_power = laserPowerInit
modNoLaser = RingModulator()
modNoLaser.laser_power *= 0

def heater_power(t):
    return t * 0.1e-3/1e-6 # 0.1 mW/us
    
def drive_voltage(t):
    return 0
    
mod.heater_power = heater_power
mod.drive_voltage = drive_voltage

modNoLaser.heater_power = heater_power
modNoLaser.drive_voltage = drive_voltage
    
#time = np.linspace(0, 10e-6, 10000)
#sol = mod.simulation_advance(time, hmax=1e-7) # set hmax smaller than bit duration
#ring_temp = sol[:,0]

format_axis(ax)

# plot resonant wavelength

heaterPowers = np.arange(0.01e-3, 1e-3, 0.003e-3)
heaterPowers = np.concatenate([heaterPowers, heaterPowers[::-1], [0]])

global time
time = 0
timeStep = 1e-3
laserLine, = ax.plot((mod._lambda_laser*1e9, mod._lambda_laser*1e9), (0, 1), color='red', label='laser wavelength')

wavelengths = np.linspace(1549e-9, 1551e-9, 400)

absorptionNoLaser = [modNoLaser.absorptionByLaserWavelength(0, wl) for wl in wavelengths]
absorptionLineNoLaser, = ax.plot(wavelengths*1e9, absorptionNoLaser, label='absorption w/o self-heating', color='lightgray')
    
absorption = [mod.absorptionByLaserWavelength(0, wl) for wl in wavelengths]
absorptionLine, = ax.plot(wavelengths*1e9, absorption, label='absorption with self-heating', color='black')


plt.legend(loc='best')

#plt.axis([0, 1, -10, 10])

axcolor = 'lightgray'
axheater = plt.axes([0.2, 0.1, 0.7, 0.03])
axlaserpower = plt.axes([0.2, 0.15, 0.7, 0.03])
axlaserwl = plt.axes([0.2, 0.20, 0.7, 0.03])

sheater = Slider(axheater, 'Heater [mw]', 0, 1, valinit=heaterInit*1e3, valfmt="%f")
slaserpower = Slider(axlaserpower, 'Laser Power [mw]', 0, 2, valinit=laserPowerInit*1e3, valfmt="%f")
slaserwl = Slider(axlaserwl, 'Laser Wavelength [nm]', 1549, 1551, valinit=1550, valfmt="%f")


def update(val):
    global time
    heaterPower = sheater.val * 1e-3
    laserPower = slaserpower.val * 1e-3
    laserWavelength = slaserwl.val * 1e-9
    
    mod._lambda_laser = laserWavelength
    modNoLaser._lambda_laser = laserWavelength
    
    mod.laser_power = laserPower
    mod.heater_power = lambda t: heaterPower
    modNoLaser.heater_power = lambda t: heaterPower
    
    mod.simulation_advance(np.linspace(time, time+timeStep, 10))
    modNoLaser.simulation_advance(np.linspace(time, time+timeStep, 10))
    
    time += timeStep
    
    absorption = [mod.absorptionByLaserWavelength(0, wl) for wl in wavelengths]
    absorptionLine.set_ydata(absorption)
    
    absorptionNoLaser = [modNoLaser.absorptionByLaserWavelength(0, wl) for wl in wavelengths]
    absorptionLineNoLaser.set_ydata(absorptionNoLaser)
    
    laserLine.set_xdata([laserWavelength*1e9, laserWavelength*1e9])
    laserLine.set_ydata([0,1])
    
    fig.canvas.draw_idle()
    
sheater.on_changed(update)
slaserpower.on_changed(update)
slaserwl.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    sheater.reset()
    slaserpower.reset()
    slaserwl.reset()
button.on_clicked(reset)

update(0)

plt.show()
